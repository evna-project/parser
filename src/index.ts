/// <reference path="./index.d.ts" />

function trimArray(array: string[]): string[] {
  return array.map(item => item.trim()).filter(item => item)
}

function processBlock(type: string, rawData: string): IBlockResult[]{
  const blockRegexp = new RegExp(`${type}.?(?:\\[(.+?)\\])?.?(?:\\((.+?)\\))?.?\\{(.+?)\\}`, "isg")

  const finalResult = []

  let regexpResult;

  while(regexpResult =  blockRegexp.exec(rawData)) {
    const result = {
      type: regexpResult[1] || null,
      props: {},
      content: ''
    }
  
    if(regexpResult[2]){
      const props = trimArray(regexpResult[2].split(','))
      const propsObject: IProps = {}
      props.forEach(item => {
        const parsedProps = item.split(':')
        propsObject[parsedProps[0]] = eval(parsedProps[1].trim())
      })
    
      result.props = propsObject
    }
  
    result.content = regexpResult[3]

    finalResult.push(result)
  }

  return finalResult
  
}

function processImports(rawData: string): IImports {
  const importsRegexp = /IMPORTS.?\{(.+?)\}/is
  const imports = importsRegexp.exec(rawData)
  if(!imports) return {}
  const items = trimArray(imports[1].split('\n'))
  const itemsObject: IStringProps = {}
  items.forEach(item => {
    const parsedItem = item.split(':')
    itemsObject[parsedItem[0].trim()] = parsedItem[1].trim()
  })
  return itemsObject
}

export default function parse(rawData: string): IParsedData{
  const parsedData = {
    name: rawData.split('\n')[0],
    imports: processImports(rawData),
    template: processBlock('template', rawData)[0] || {},
    scripts: processBlock('script', rawData),
    styles: processBlock('style', rawData),
  }

  return parsedData

}