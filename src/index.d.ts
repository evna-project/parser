interface IProps {
  [key: string]: string | number
}

interface IStringProps {
  [key: string]: string
}

interface IImports {
  [key: string]: string
}

interface IBlockResult {
  type: string | null
  props: IProps,
  content: string
}

interface IParsedData {
  name: string
  imports: IImports
  template: IBlockResult
  scripts: IBlockResult[]
  styles: IBlockResult[]
}